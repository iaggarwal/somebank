import { Observable, of, throwError } from 'rxjs';
import { CreditCard } from '../types/card.model';
import Axios from 'axios-observable';
import { map, catchError } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import { ErrorResponse } from '../types/error.models';

export class CreditCardApiService {
  static getAllcreditCards(): Observable<Array<CreditCard> | any> {
    return Axios.get<Array<CreditCard>>(
      'http://localhost:8080/api/v1/creditcard'
    ).pipe(
      map((response: AxiosResponse<Array<CreditCard>>) => response.data)
    );
  }

  static addCreditCard(card: CreditCard): Observable<CreditCard | string> {
    return Axios.post<CreditCard>(
      'http://localhost:8080/api/v1/creditcard',
      card
    ).pipe(
      map((response: AxiosResponse<CreditCard>) => response.data)
    );
  }
}
