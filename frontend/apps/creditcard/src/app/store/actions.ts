import { CreditCard } from '../types/card.model';
import { Action } from 'redux';
import { ErrorResponse } from '../types/error.models';

export enum CreditCardActionTypes {
  ADD_CARD = '[Credit Card] - [Add]',
  ADD_CARD_SUCCESS = '[Credit Card] - [Add] success',
  ADD_CARD_ERROR = '[Credit Card] - [Add] error',
  LIST_CARD = '[Credit Card] - [List]',
  LIST_CARD_SUCCESS = '[Credit Card] - [List] success',
  LIST_CARD_ERROR = '[Credit Card] - [List] error'
}

export class AddCard implements Action {
  readonly type = CreditCardActionTypes.ADD_CARD;
  constructor(public payload: CreditCard) {}
}

export class AddCardSuccess implements Action {
  readonly type = CreditCardActionTypes.ADD_CARD_SUCCESS;
  constructor(public payload: CreditCard) {}
}
export class AddCardError implements Action {
  readonly type = CreditCardActionTypes.ADD_CARD_ERROR;
  constructor(public payload: ErrorResponse) {}
}
export class ListCard implements Action {
  readonly type = CreditCardActionTypes.LIST_CARD;
}
export class ListCardSuccess implements Action {
  readonly type = CreditCardActionTypes.LIST_CARD_SUCCESS;
  constructor(public payload: Array<CreditCard>) {}
}
export class ListCardError implements Action {
  readonly type = CreditCardActionTypes.LIST_CARD_ERROR;
  constructor(public payload: ErrorResponse) {}
}

export type CreditCardAction =
  | AddCard
  | AddCardSuccess
  | AddCardError
  | ListCard
  | ListCardSuccess
  | ListCardError;
