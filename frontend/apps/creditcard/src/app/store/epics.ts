import {
  AddCardError,
  AddCardSuccess,
  AddCard,
  CreditCardActionTypes,
  ListCard,
  ListCardSuccess,
  ListCardError
} from './actions';
import { ofType, ActionsObservable } from 'redux-observable';
import { map, switchMap, catchError } from 'rxjs/operators';
import { CreditCardApiService } from '../services/credit-card.api.services';
import { CreditCard } from '../types/card.model';
import { Observable, of } from 'rxjs';

export const addCardEpic = (
  action$: ActionsObservable<AddCard>
): Observable<AddCardSuccess | AddCardError> =>
  action$.pipe(
    ofType(CreditCardActionTypes.ADD_CARD),
    switchMap((action: AddCard) => {
      return CreditCardApiService.addCreditCard(action.payload).pipe(
        map(response => ({ ...new AddCardSuccess(response as CreditCard) })),
        catchError(error => {
          return of({ ...new AddCardError(error) });
        })
      );
    })
  );

export const addCardSuccessEpic = (
  action$: ActionsObservable<AddCardSuccess>
): Observable<ListCard> =>
  action$.pipe(
    ofType(CreditCardActionTypes.ADD_CARD_SUCCESS),
    map((action: AddCardSuccess) => {
      return { ...new ListCard() };
    })
  );

export const listAllCardsEpic = (
  action$: ActionsObservable<ListCard>
): Observable<ListCardSuccess | ListCardError> =>
  action$.pipe(
    ofType(CreditCardActionTypes.LIST_CARD),
    switchMap((action: ListCard) => {
      return CreditCardApiService.getAllcreditCards().pipe(
        map(response => ({
          ...new ListCardSuccess(response as Array<CreditCard>)
        })),
        catchError(error => of({ ...new ListCardError(error) }))
      );
    })
  );
