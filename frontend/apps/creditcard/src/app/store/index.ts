import { CreditCard } from '../types/card.model';
import { combineReducers, applyMiddleware, createStore } from 'redux';
import { creditCardReducer } from './reducer';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { addCardEpic, listAllCardsEpic, addCardSuccessEpic } from './epics';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ErrorResponse } from '../types/error.models';

export * from './actions';
export * from './epics';
export * from './reducer';

export interface ListState {
  loading: Boolean;
  cards: Array<CreditCard>;
  errorMessage: ErrorResponse;
}

export interface CreateState {
  creating: Boolean;
  errorMessage: ErrorResponse;
}

export interface CreditCardRootState {
  list: ListState;
  create: CreateState;
}

const rootEpic = combineEpics<any>(
  addCardEpic,
  listAllCardsEpic,
  addCardSuccessEpic
);

const epicMiddleware = createEpicMiddleware();

export const store = createStore(
  creditCardReducer,
  composeWithDevTools(applyMiddleware(epicMiddleware))
);

epicMiddleware.run(rootEpic);
