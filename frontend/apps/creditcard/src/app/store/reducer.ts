import { CreditCard } from '../types/card.model';
import { CreditCardAction, CreditCardActionTypes } from './actions';
import { CreditCardRootState } from '.';

const initialState: CreditCardRootState = {
  list: {
    loading: false,
    cards: [],
    errorMessage: undefined
  },
  create: {
    creating: false,
    errorMessage: undefined
  }
};

export const creditCardReducer = (
  state: CreditCardRootState = initialState,
  action: CreditCardAction
) => {
  switch (action.type) {
    case CreditCardActionTypes.ADD_CARD: {
      return {
        ...state,
        create: {
          ...state.create,
          creating: true,
          errorMessage: undefined
        }
      };
    }
    case CreditCardActionTypes.ADD_CARD_SUCCESS: {
      return {
        ...state,
        create: {
          ...state.create,
          creating: false,
          errorMessage: undefined
        }
      };
    }
    case CreditCardActionTypes.ADD_CARD_ERROR: {
      return {
        ...state,
        create: {
          ...state.create,
          creating: false,
          errorMessage: {
            ...action.payload
          }
        }
      };
    }
    case CreditCardActionTypes.LIST_CARD: {
      return {
        ...state,
        list: {
          ...state.list,
          loading: true,
          errorMessage: undefined
        }
      };
    }
    case CreditCardActionTypes.LIST_CARD_SUCCESS: {
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          errorMessage: undefined,
          cards: [...action.payload]
        }
      };
    }
    case CreditCardActionTypes.LIST_CARD_ERROR: {
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          errorMessage: {
            ...state.create.errorMessage,
            ...action.payload
          },
          cards: []
        }
      };
    }
    default:
      return { ...state };
  }
};
