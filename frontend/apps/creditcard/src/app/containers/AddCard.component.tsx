import React, { FC, useState } from 'react';
import { CreditCardRootState, AddCard } from '../store';
import { CreditCard } from '../types/card.model';
import { connect } from 'react-redux';
import {
  FormControl,
  Select,
  TextField,
  MenuItem,
  InputAdornment,
  FormHelperText,
  Button,
  Typography,
  CircularProgress,
  Box
} from '@material-ui/core';
import {
  getComponentAsFlexRowContainer,
  FlexRowContainer
} from '../components/FlexContainer.component';
import styled from 'styled-components';
import { getFormFieldWithSpacing } from '../components/FormField.component';
import { useForm } from 'react-hook-form';
import { ErrorResponse } from '../types/error.models';

export interface AddCardComponentPropsType {
  creating: Boolean;
  errorResponse: ErrorResponse;
  addCard: Function;
}

const FlexContainerForm = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  justify-content: space-between;
  align-items: flex-start;
  min-width: 200px;
`;

const StyledFormControl = styled(FormControl)`
  width: 90%;
  margin-bottom: 7.5px;
`;

export const FlexColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const StyledFormHelperText = styled(FormHelperText)`
  padding-left: 15px;
`;

const StyledErrorBox = styled(Box)`
  flex: 1 1 50%;
  padding: 7.5px;
  border-radius: 10px;
`;

export const AddCardComponent: FC<AddCardComponentPropsType> = ({
  creating,
  errorResponse,
  addCard
}) => {
  const getErrorHealper = message => (
    <StyledFormHelperText error={true}>{message}</StyledFormHelperText>
  );

  const displayBadRequest = (error: ErrorResponse) =>
    error.data.map(fieldError => {
      return (
        <FlexColumnContainer
          key={`${fieldError.field}-${fieldError.defaultMessage}`}
        >
          <Typography variant="body1" component="div">
            {fieldError.defaultMessage}
          </Typography>
        </FlexColumnContainer>
      );
    });

  const {
    register,
    handleSubmit,
    formState,
    errors,
    triggerValidation,
    reset
  } = useForm({
    mode: 'onChange'
  });
  const [limit, setLimit] = useState(100);
  const [name, setName] = useState('');
  const [creditCard, setCreditCard] = useState('');
  const handelOnSubmit = data => {
    addCard({ ...data, balance: 0 });
    reset();
  };

  return (
    <>
      <Typography variant="h5" component="h3">
        Add
      </Typography>
      <FlexRowContainer>
        <FlexColumnContainer>
          <FlexContainerForm
            onSubmit={handleSubmit(handelOnSubmit)}
            noValidate
            autoComplete="off"
          >
            <StyledFormControl>
              <TextField
                required
                fullWidth={true}
                variant="outlined"
                inputRef={register({ required: 'Name is a required field' })}
                name="name"
                label="Name"
                aria-describedby="name-on-card-helper"
                helperText="This is the name on the credit card. This is a required field"
              />
              {errors.name && getErrorHealper(errors.name.message)}
            </StyledFormControl>
            <StyledFormControl>
              <TextField
                required
                fullWidth={true}
                variant="outlined"
                type="number"
                inputRef={register({
                  required: 'Credit card number is a required field',
                  min: {
                    value: 0,
                    message: 'Card number cannot be a negative number'
                  },
                  minLength: {
                    value: 13,
                    message: 'Card number cannot be smaller than 13 digits'
                  },
                  maxLength: {
                    value: 19,
                    message: 'Card number cannot be greater than 19 digits'
                  }
                })}
                name="cardNumber"
                label="Card number"
                aria-describedby="card-number-helper"
                helperText="This is the number of the credit card. It will be validated by Lhun
            10 algorithm"
              />
              {errors.cardNumber && getErrorHealper(errors.cardNumber.message)}
            </StyledFormControl>
            <StyledFormControl>
              <TextField
                required
                fullWidth={true}
                variant="outlined"
                inputRef={register({
                  required: 'Limit is a required field',
                  min: {
                    value: 100,
                    message: 'Credit limit should be greater than £100'
                  },
                  max: {
                    value: 5000,
                    message: 'Credit limit should be lesser than £5000'
                  }
                })}
                type="number"
                name="limit"
                label="Limit"
                aria-describedby="limit-helper"
                helperText="This is the limit of the credit card and cannot be less than £100 or
            more than £500"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">£</InputAdornment>
                  )
                }}
              />
              {errors.limit && getErrorHealper(errors.limit.message)}
            </StyledFormControl>
            <Button
              type="submit"
              fullWidth={false}
              variant="contained"
              color="primary"
              disabled={!formState.isValid}
            >
              Add
            </Button>
            {creating && <CircularProgress size={24} />}
          </FlexContainerForm>
        </FlexColumnContainer>
        {errorResponse && (
          <StyledErrorBox
            border={1}
            borderColor="error.main"
            boxShadow={1}
            component="div"
            color="primary"
          >
            {displayBadRequest(errorResponse)}
          </StyledErrorBox>
        )}
      </FlexRowContainer>
    </>
  );
};

const mapStateToProps = (state: CreditCardRootState) => {
  return {
    creating: state && state.create && state.create.creating,
    errorResponse: state && state.create && state.create.errorMessage
  };
};

const mapPropsToDispatch = dispatch => {
  return {
    addCard: (card: CreditCard) => dispatch({ ...new AddCard(card) })
  };
};

export default connect(mapStateToProps, mapPropsToDispatch)(AddCardComponent);
