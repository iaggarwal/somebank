import React, { FC } from 'react';
import { CreditCard } from '../types/card.model';
import { CreditCardRootState } from '../store';
import { connect } from 'react-redux';
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography
} from '@material-ui/core';
import { NumberFormatterService } from '../services/number-formatter.service';
import { ErrorResponse } from '../types/error.models';

export interface ListCardComponentPropsType {
  loading: Boolean;
  cardList: Array<CreditCard>;
  errorResponse: ErrorResponse;
}
export const ListCardComponent: FC<ListCardComponentPropsType> = ({
  loading,
  cardList,
  errorResponse
}) => {
  return (
    <>
      <Typography variant="h5" component="h3">
        Existing cards
      </Typography>
      <TableContainer component={Paper}>
        <Table aria-label="This is the table for displaying list of credit cards">
          <caption>Table for displaying list of credit cards</caption>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="left">Card number</TableCell>
              <TableCell align="left">Balance</TableCell>
              <TableCell align="left">Limit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {cardList.map(row => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="left">{row.cardNumber}</TableCell>
                <TableCell align="left">
                  {NumberFormatterService.toCurrency('GBP', row.balance)}
                </TableCell>
                <TableCell align="left">
                  {NumberFormatterService.toCurrency('GBP', row.limit)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

const mapStateToProps = (state: CreditCardRootState) => {
  return {
    loading: state && state.list && state.list.loading,
    cardList: (state && state.list && state.list.cards) || [],
    errorResponse: (state && state.list && state.list.errorMessage) || []
  };
};

export default connect(mapStateToProps, null)(ListCardComponent);
