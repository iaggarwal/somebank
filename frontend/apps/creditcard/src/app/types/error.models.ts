export interface ErrorResponse {
  status: string;
  errorCode: string;
  data: Array<any>;
}
