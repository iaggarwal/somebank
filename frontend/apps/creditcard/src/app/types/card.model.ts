export interface CreditCard {
  cardNumber: string;
  name: string;
  limit: number;
  balance: number;
}
