import styled from 'styled-components';
import { FC } from 'react';

export const FlexColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FlexRowContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 7.5px;
`;

export const getComponentAsFlexColumnContainer = (component: FC<any>) => styled(
  component
)`
  display: flex;
  flex-direction: column;
`;

export const getComponentAsFlexRowContainer = (component) => styled(
  component
)`
  display: flex;
  flex-direction: row;
`;
