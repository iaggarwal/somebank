import styled from 'styled-components';

export const getFormFieldWithSpacing = (component) => styled(
    component
  )`
    margin-top: 5px;
  `;
  