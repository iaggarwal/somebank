import styled from 'styled-components';
import { FC } from 'react';

export const getComponentAsFlexItem = (component: FC<any>) => styled(
  component
)`
  flex: 1 1 auto;
`;
