import React, { FC } from 'react';

import styled from 'styled-components';
import { Provider } from 'react-redux';
import { store, ListCard } from './store';
import AddCardComponent from './containers/AddCard.component';
import ListCardComponent from './containers/ListCard.component';
import { getComponentAsFlexItem } from './components/FlexItem.component';
import { Typography, Divider, StylesProvider, CssBaseline } from '@material-ui/core';
import { addResponseInterceptor } from './interceptors/response.interceptor';

const FlexItemSmall = styled.div`
  flex: 1 1 40%;
  margin-top: 30px;
  margin-bottom: 30px;
`;

const FlexItemLarge = styled.div`
  flex: 1 1 60%;
  height: 60%;
  margin-top: 30px;
`;

export const FlexColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 98vh;
  padding: 15px;
`;

export const App: FC = () => {
  addResponseInterceptor();
  store.dispatch({ ...new ListCard() });
  return (
    <Provider store={store}>
      <StylesProvider injectFirst>
        <CssBaseline />
        <FlexColumnContainer>
          <Typography variant="h3" component="h2" gutterBottom>
            Credit Card System
          </Typography>
          <Divider />
          <FlexItemSmall>
            <AddCardComponent />
          </FlexItemSmall>
          <Divider />
          <FlexItemLarge>
            <ListCardComponent />
          </FlexItemLarge>
        </FlexColumnContainer>
      </StylesProvider>
    </Provider>
  );
};

export default App;
