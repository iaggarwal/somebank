import Axios from 'axios';
import { ErrorResponse } from '../types/error.models';

export const addResponseInterceptor = () => {
  Axios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      if (error.response.status === 400) {
        const response: ErrorResponse = {
          status: error.response.status,
          errorCode: 'BAD_REQUEST',
          data: error.response.data.errors.map(({ defaultMessage, field }) => {
            return {
              defaultMessage,
              field
            };
          })
        };
        return Promise.reject(response);
      } else if (error.response.status === 412) {
        const response: ErrorResponse = {
          status: error.response.status,
          errorCode: 'PRECONDITION_FAILED',
          data: [
            {
              defaultMessage: error.response.data.message,
              field: undefined
            }
          ]
        };
        return Promise.reject(response);
      }
      return Promise.reject(error.response);
    }
  );
};
