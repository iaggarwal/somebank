# Backend

This application can be run either in IDEs like InteliJ and Eclipse or through command line. If running vis command line then maven needs to be installed. Once maven is installed please run:

* mvn package - This will create a .jar file in the `./target` directory
* `java -jar <nameOfTheJarGenerateAbove>`

With this the server should be running on port 8080. If the front end application is not able to connect to this port that most probably means firewall is blocking the access. Google it and you will find how to resolve it.