package com.somebank.plasticmoney.creditcard.dao;

import com.somebank.plasticmoney.creditcard.model.CreditCard;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest(classes = CreditCardDaoImpl.class)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CreditCardDaoTests {

    private final String correctCreditCardNumber = "378282246310005";

    @Autowired()
    @Qualifier("inMemoryDAO")
    private CreditCardDao creditCardDao;

    @Test
    public void addCardTest() {
        this.creditCardDao.addCard(
                new CreditCard(correctCreditCardNumber,
                        "Ishan Aggarwal",
                        new Date(),
                        new Date(),
                        0,
                        5000));
        Assertions.assertThat(this.creditCardDao.getAllCards().size()).isEqualTo(1);
        Assertions.assertThat(this.creditCardDao.getAllCards().get(0).getCardNumber()).isEqualTo(correctCreditCardNumber);
    }

    @Test
    public void getAllCardsTest() {
        Assertions.assertThat(this.creditCardDao.getAllCards().size()).isEqualTo(0);
        this.creditCardDao.addCard(
                new CreditCard(correctCreditCardNumber,
                        "Ishan Aggarwal",
                        new Date(),
                        new Date(),
                        0,
                        5000));
        Assertions.assertThat(this.creditCardDao.getAllCards().size()).isEqualTo(1);
    }

    @Test
    public void isExistingCardTest() {
        this.creditCardDao.addCard(
                new CreditCard(correctCreditCardNumber,
                        "Ishan Aggarwal",
                        new Date(),
                        new Date(),
                        0,
                        5000));
        Assertions.assertThat(this.creditCardDao.isExistingCard(correctCreditCardNumber)).isTrue();
    }
}
