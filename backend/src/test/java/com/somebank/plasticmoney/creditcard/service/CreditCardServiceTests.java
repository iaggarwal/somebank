package com.somebank.plasticmoney.creditcard.service;

import com.somebank.plasticmoney.creditcard.dao.CreditCardDao;
import com.somebank.plasticmoney.creditcard.dao.CreditCardDaoImpl;
import com.somebank.plasticmoney.creditcard.model.CreditCard;
import com.somebank.plasticmoney.exceptions.ExistingCardException;
import com.somebank.utilities.Lhun;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.verification.VerificationMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = CreditCardService.class)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = CreditCardDaoImpl.class)
public class CreditCardServiceTests {
    private final String correctCreditCardNumber = "378282246310005";

    @Autowired
    CreditCardService creditCardService;

    @SpyBean
    private CreditCardDaoImpl creditCardDaoImpl;

    @Test
    public void addNonExistingCreditCardTest() {
        CreditCard card = new CreditCard(correctCreditCardNumber,
                "Ishan Aggarwal",
                new Date(),
                new Date(),
                0,
                5000);
        given(this.creditCardDaoImpl.addCard( card )).willReturn(1);

        given(this.creditCardDaoImpl.isExistingCard(correctCreditCardNumber)).willReturn(false);
        this.creditCardService.addCreditCard(new CreditCard(correctCreditCardNumber,
                "Ishan Aggarwal",
                new Date(),
                new Date(),
                0,
                5000));
        verify(creditCardDaoImpl, times(1)).isExistingCard(correctCreditCardNumber);
        verify(creditCardDaoImpl, times(1)).addCard(card);
    }

    @Test
    public void addExistingCreditCardTest() {
        CreditCard card = new CreditCard(correctCreditCardNumber,
                "Ishan Aggarwal",
                new Date(),
                new Date(),
                0,
                5000);
        given(this.creditCardDaoImpl.isExistingCard(correctCreditCardNumber)).willReturn(true);
        Throwable exception = assertThrows(ExistingCardException.class, () -> this.creditCardService.addCreditCard(card));
        Assertions.assertThat(exception.getMessage()).isEqualTo("Card number already exists");
        verify(creditCardDaoImpl, times(1)).isExistingCard(correctCreditCardNumber);
        verify(creditCardDaoImpl, never()).addCard(card);
    }

    @Test
    public void getAllCardsTest() {
        given(this.creditCardDaoImpl.getAllCards()).willReturn(new ArrayList<>());
        this.creditCardService.getAllCreditCards();
        verify(creditCardDaoImpl, times(1)).getAllCards();
    }

    @Test
    public void isExistingCardTest() {
        given(this.creditCardDaoImpl.isExistingCard(correctCreditCardNumber)).willReturn(true);
        this.creditCardService.isExistingCard(correctCreditCardNumber);
        verify(creditCardDaoImpl, times(1)).isExistingCard(correctCreditCardNumber);
    }
}
