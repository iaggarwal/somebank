package com.somebank.utilities;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = Lhun.class)
@RunWith(SpringRunner.class)
public class LhunTests {

    @Test
    public void testCorrectCreditCardNumber() {
        String correctCreditCardNumber = "378282246310005";
        Boolean result = Lhun.check(correctCreditCardNumber);
        Assertions.assertThat(result).isEqualTo(true);
    }

    @Test
    public void testInCorrectCreditCardNumber() {
        String inCorrectCreditCardNumber = "378282246310004";
        Boolean result = Lhun.check(inCorrectCreditCardNumber);
        Assertions.assertThat(result).isEqualTo(false);
    }
}
