package com.somebank.plasticmoney.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class ExistingCardException extends RuntimeException {
    public ExistingCardException(String exception) {
        super(exception);
    }
}
