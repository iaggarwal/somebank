package com.somebank.plasticmoney.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.somebank.plasticmoney.validators.cardNumber.CardNumberConstraint;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class Card {
    @NotNull
    @CardNumberConstraint
    private final String cardNumber;
    @NotBlank
    private final String name;
    private final Date start;
    private final Date expiry;
    private final float balance;

    public float getBalance() {
        return balance;
    }

    public Card(String cardNumber,
                String name,
                Date start,
                Date expiry,
                float balance) {
        this.cardNumber = cardNumber;
        this.name = name;
        this.start = start;
        this.expiry = expiry;
        this.balance = balance;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getName() {
        return name;
    }

    public Date getStart() {
        return start;
    }

    public Date getExpiry() {
        return expiry;
    }
}
