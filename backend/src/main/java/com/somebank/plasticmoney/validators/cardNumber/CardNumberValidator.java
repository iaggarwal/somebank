package com.somebank.plasticmoney.validators.cardNumber;

import com.somebank.utilities.Lhun;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CardNumberValidator implements ConstraintValidator<CardNumberConstraint, String> {
    @Override
    public void initialize(CardNumberConstraint creditCardNumber) {
    }

    @Override
    public boolean isValid(String creditCardNumber,
                           ConstraintValidatorContext cxt) {
        return creditCardNumber != null && creditCardNumber.matches("[0-9]+")
                && (creditCardNumber.length() > 12) && (creditCardNumber.length() < 20) && Lhun.check(creditCardNumber);
    }
}