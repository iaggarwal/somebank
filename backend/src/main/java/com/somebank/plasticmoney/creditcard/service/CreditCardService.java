package com.somebank.plasticmoney.creditcard.service;

import com.somebank.plasticmoney.creditcard.dao.CreditCardDao;
import com.somebank.plasticmoney.creditcard.model.CreditCard;
import com.somebank.plasticmoney.exceptions.ExistingCardException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditCardService {

    private final CreditCardDao creditCardDao;

    @Autowired
    public CreditCardService(@Qualifier("inMemoryDAO") CreditCardDao creditCardDao) {
        this.creditCardDao = creditCardDao;
    }

    public int addCreditCard(CreditCard creditCard) throws ExistingCardException {
        if (this.isExistingCard(creditCard.getCardNumber())) {
            throw new ExistingCardException("Card number already exists");
        }
        return this.creditCardDao.addCard(creditCard);
    }

    public List<CreditCard> getAllCreditCards() {
        return this.creditCardDao.getAllCards();
    }

    public Boolean isExistingCard (String cardNumber) {
        return this.creditCardDao.isExistingCard(cardNumber);
    }
}
