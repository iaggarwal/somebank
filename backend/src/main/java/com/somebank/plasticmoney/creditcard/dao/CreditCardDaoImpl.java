package com.somebank.plasticmoney.creditcard.dao;

import com.somebank.plasticmoney.creditcard.model.CreditCard;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("inMemoryDAO")
public class CreditCardDaoImpl implements CreditCardDao {

    private static Map<String, CreditCard> DB = new HashMap<>();
    private static Map<String, Number> CVV_DB = new HashMap<>();

    @Override
    public int addCard(Date start, Date expiry, Number cvv, CreditCard creditCard) {
        CreditCardDaoImpl.DB.put(creditCard.getCardNumber(), new CreditCard(creditCard.getCardNumber(),
                creditCard.getName(),
                start,
                expiry,
                0.0f,
                creditCard.getLimit()));
        CreditCardDaoImpl.CVV_DB.put(creditCard.getCardNumber(), cvv);
        return 1;
    }

    @Override
    public List<CreditCard> getAllCards() {
        return new ArrayList<>(CreditCardDaoImpl.DB.values());
    }

    @Override
    public Boolean isExistingCard(String cardNumber) {
        return CreditCardDaoImpl.DB.containsKey(cardNumber);
    }
}
