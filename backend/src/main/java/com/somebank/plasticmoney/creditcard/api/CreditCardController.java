package com.somebank.plasticmoney.creditcard.api;

import com.somebank.plasticmoney.creditcard.model.CreditCard;
import com.somebank.plasticmoney.creditcard.service.CreditCardService;
import com.somebank.plasticmoney.exceptions.ExistingCardException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@CrossOrigin("http://localhost:4200")
@RequestMapping("/api/v1/creditcard")
@RestController
public class CreditCardController {
    private final CreditCardService creditCardService;

    @Autowired
    public CreditCardController(CreditCardService creditCardService) {
        this.creditCardService = creditCardService;
    }

    @PostMapping
    public void addCreditCard(@Valid @NotNull @RequestBody CreditCard creditCard) {
        if (this.creditCardService.isExistingCard(creditCard.getCardNumber())) {
            throw new ExistingCardException("Card number already exists");
        }
        this.creditCardService.addCreditCard(creditCard);
    }

    @GetMapping
    public List<CreditCard> getAllCreditCards() {
        return this.creditCardService.getAllCreditCards();
    }
}
