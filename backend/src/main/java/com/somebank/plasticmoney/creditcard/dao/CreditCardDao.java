package com.somebank.plasticmoney.creditcard.dao;

import com.somebank.plasticmoney.creditcard.model.CreditCard;

import java.util.*;

public interface CreditCardDao {
    int addCard(Date start, Date expiry, Number cvv, CreditCard creditCard);
    default int addCard(CreditCard creditCard) {
        Date start = new Date();
        this.addCard(start, this.getExpiryDate(start), this.getCvv(), creditCard);
        return 1;
    }
    List<CreditCard> getAllCards();

    Boolean isExistingCard(String cardNumber);

    private Date getExpiryDate(Date start) {
        Calendar c = Calendar.getInstance();
        c.setTime(start);
        c.add(Calendar.YEAR, 3);
        return c.getTime();
    }

    private Number getCvv() {
        Random random = new Random();
        return random.nextInt(900) + 100;
    }
}
