package com.somebank.plasticmoney.creditcard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.somebank.plasticmoney.model.Card;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class CreditCard extends Card {

    @NotNull
    private final float limit;

    public CreditCard(@JsonProperty("cardNumber") String cardNumber,
                      @JsonProperty("name") String name,
                      @JsonProperty("start") Date start,
                      @JsonProperty("expiry") Date expiry,
                      @JsonProperty("balance") float balance,
                      @JsonProperty("limit") float limit) {
        super(cardNumber, name, start, expiry, balance);
        this.limit = limit;
    }

    public float getLimit() {
        return limit;
    }
}
