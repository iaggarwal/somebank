package com.somebank.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Lhun {
    public static Boolean check(String cardNumber) {
        List<String> checkSumList = new ArrayList<String>(Arrays.asList(cardNumber.split("")));
        Collections.reverse(checkSumList);
        List<Integer> integerList = checkSumList.stream().map(Integer::parseInt).collect(Collectors.toList());
        Integer checkSum = IntStream
                .range(0, integerList.size())
                .reduce(0, (int sum, int index) -> {
                    Integer additive = index % 2 == 0 ? integerList.get(index) : integerList.get(index) * 2;
                    sum += additive > 9 ? additive - 9 : additive;
                    return sum;
                });
        return checkSum % 10 == 0;
    }
}
